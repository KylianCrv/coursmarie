function convDistance() {
    //APPEL DE LA METHODE EN PHP
    jQuery.ajax({
        type: "POST",
        url: 'php/conversions_distance.php',
        dataType: 'json',
        data: {distance: document.getElementById('distanceVal').value, 
                uniteEntree:document.getElementById('uniteEntree').value, 
                uniteSortie:document.getElementById('uniteSortie').value},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                console.log(obj.result);
            } else {
                console.log(obj.error);
            }
        }
    });
    //AFFICHAGE DU RESULTAT DANS LA PAGE
    document.getElementById('resultat_distance').innerHTML = 'xx unit1 équivaut à xx unit2.';
}

