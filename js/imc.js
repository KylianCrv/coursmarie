function calcIMC() {
    //APPEL DE LA METHODE EN PHP
    jQuery.ajax({
        type: "POST",
        url: 'php/calcul_imc.php',
        dataType: 'json',
        data: { poids: document.getElementById('poidsValue').value, 
                unitePoids:document.getElementById('unitePoids').value, 
                taille: document.getElementById('tailleValue').value, 
                uniteTaille:document.getElementById('uniteTaille').value},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                console.log(obj.result);
            } else {
                console.log(obj.error);
            }
        }
    });
    //AFFICHAGE DU RESULTAT DANS LA PAGE
    document.getElementById('resultat_imc').innerHTML = 'Votre imc est de xx. Vous êtes en super forme!';
}

