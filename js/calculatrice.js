var op = [];
var opString = '';

function initCalc() {
    op = [];
    display();
}

function deleteLastEl() {
    op.splice(op.length - 1, 1);
    display();
}

function addToTab(val) {
    op.push(val);
    display();
}

function calc() {
    //APPEL DE LA METHODE EN PHP
    jQuery.ajax({
        type: "POST",
        url: 'php/calculatrice.php',
        dataType: 'json',
        data: {tab: op.join('')},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                console.log(obj.result);
            } else {
                console.log(obj.error);
            }
        }
    });
    //AFFICHAGE DU RESULTAT DANS LA CALCULATRICE
    document.getElementById('calc_resultat').value = 'res';
}

function display() {
    opString = op.join('');
    document.getElementById('calc_resultat').value = opString;
}