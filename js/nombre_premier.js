function calcNbPremier() {
    //APPEL DE LA METHODE EN PHP
    jQuery.ajax({
        type: "POST",
        url: 'php/nombre_premier.php',
        dataType: 'json',
        data: {nb: document.getElementById("nbPremier").value},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                console.log(obj.result);
            } else {
                console.log(obj.error);
            }
            //AFFICHAGE DU RESULTAT
            document.getElementById('nb_premier_resultat').innerHTML = obj.result;
        }
    });

}

function reset() {
    document.getElementById('nbPremier').value = '';
    document.getElementById("nb_premier_resultat").innerHTML = '';
}