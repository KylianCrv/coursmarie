function calcVitesse() {
    //APPEL DE LA METHODE EN PHP
    jQuery.ajax({
        type: "POST",
        url: 'php/calcul_vitesse.php',
        dataType: 'json',
        data: { distance: document.getElementById('distanceValue').value, 
                uniteDistance:document.getElementById('uniteDistance').value, 
                temps: document.getElementById('tempsValue').value, 
                uniteTemps:document.getElementById('uniteTemps').value},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                console.log(obj.result);
            } else {
                console.log(obj.error);
            }
        }
    });
    //AFFICHAGE DU RESULTAT DANS LA PAGE
    document.getElementById('resultat_vitesse').innerHTML = 'Vous allez à xx km/h.';
}

