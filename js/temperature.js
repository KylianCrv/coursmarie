function convTemperature() {
    //APPEL DE LA METHODE EN PHP
    jQuery.ajax({
        type: "POST",
        url: 'php/conversions_temperature.php',
        dataType: 'json',
        data: {temperature: document.getElementById('temperatureVal').value, unite:document.getElementById('unite').value},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                console.log(obj.result);
            } else {
                console.log(obj.error);
            }
        }
    });
    //AFFICHAGE DU RESULTAT DANS LA PAGE
    //unit1 est °C si unit2 est F et inversement
    document.getElementById('resultat_temperature').innerHTML = 'xx unit1 équivaut à xx unit2.';
}

