<?php
header('Content-Type: application/json');
require_once "class\Age.php";

if (!isset($_POST['date'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = Age::calcAge($_POST['nb']);
    $res['result'] = $result;
}
