<?php
header('Content-Type: application/json');
require_once 'class\Conversions.php';

if (!isset($_POST['distance']) && !isset($_POST['unite'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = Conversions::calcDistance($_POST['distance'], $_POST['unit']);
    $res['result'] = $result;
}

echo json_encode($res);
