<?php
header('Content-Type: application/json');

require_once "class\NombrePremier.php";

if (!isset($_POST['nb'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = NombrePremier::isNombrePremier($_POST['nb']);
    $res['result'] = $result;
}

echo json_encode($res);



