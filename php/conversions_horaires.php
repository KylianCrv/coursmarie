<?php
header('Content-Type: application/json');
require_once 'class\Conversions.php';

if (!isset($_POST['heure']) && !isset($_POST['unite'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = Conversions::calcHoraire($_POST['heure'], $_POST['unit']);
    $res['result'] = $result;
}

echo json_encode($res);
