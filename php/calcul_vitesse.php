<?php
header('Content-Type: application/json');

if (!isset($_POST['distance']) && !isset($_POST['uniteDistance']) 
        && !isset($_POST['temps']) && !isset($_POST['uniteTemps'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = calcVitesse($_POST['distance'], $_POST['uniteDistance'], $_POST['temps'], $_POST['uniteTemps']);
    $res['result'] = $result;
}

function calcVitesse($distance, $uniteDistance, $temps, $uniteTemps){
    return 'res';
}

echo json_encode($res);