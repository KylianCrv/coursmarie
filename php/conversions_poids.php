<?php
header('Content-Type: application/json');
require_once 'class\Conversions.php';

if (!isset($_POST['poids']) && !isset($_POST['unite'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = Conversions::calcPoids($_POST['poids'], $_POST['unit']);
    $res['result'] = $result;
}

echo json_encode($res);