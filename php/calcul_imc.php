<?php
header('Content-Type: application/json');
require_once "class\IMC.php";

if (!isset($_POST['poids']) && !isset($_POST['unitePoids']) 
        && !isset($_POST['taille']) && !isset($_POST['uniteTaille'])) {
    $res['error'] = 'No function arguments!';
}

if (!isset($res['error'])) {
    $result = IMC::calcIMC($_POST['poids'], $_POST['unitePoids'], $_POST['taille'], $_POST['uniteTaille']);
    $res['result'] = $result;
}

echo json_encode($res);