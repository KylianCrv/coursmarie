<?php

class Conversions {

    /**
     * Convertit une distance dans une unité choisie
     * @param type $distance
     * @param type $unite
     * @return string
     */
    public static function calcDistance($distance, $unite) {
        return 'res';
    }

    /**
     * Convertit une heure dans une unité choisie
     * @param type $heure
     * @param type $unite
     * @return string
     */
    public static function calcHoraires($heure, $unite) {
        return 'res';
    }

    /**
     * Convertit un poids dans une unité choisie
     * @param type $poids
     * @param type $unite
     * @return string
     */
    public static function calcPoids($poids, $unite) {
        return 'res';
    }

    /**
     * Convertit une température dans une unité choisie
     * @param type $temperature
     * @param type $unite
     * @return string
     */
    public static function calcTemperature($temperature, $unite) {
        return 'res';
    }

}
