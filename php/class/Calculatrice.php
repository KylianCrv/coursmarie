<?php

class Calculatrice {

    /**
     * Ajoute deux nombres entre eux
     * @param type $nb1
     * @param type $nb2
     * @return string
     */
    public static function add($nb1, $nb2) {
        return 'res';
    }
    
    /**
     * Soustrait deux nombres entre eux
     * @param type $nb1
     * @param type $nb2
     * @return string
     */
    public static function sub($nb1, $nb2) {
        return 'res';
    }
    
    /**
     * Multiplie deux nombres entre eux
     * @param type $nb1
     * @param type $nb2
     * @return string
     */
    public static function mult($nb1, $nb2) {
        return 'res';
    }
    
    /**
     * Divise deux nombres entre eux
     * @param type $nb1
     * @param type $nb2
     * @return string
     */
    public static function div($nb1, $nb2) {
        return 'res';
    }
    /**
     * Retourne le modulo de deux nombres
     * @param type $nb1
     * @param type $nb2
     * @return string
     */
    public static function mod($nb1, $nb2) {
        return 'res';
    }
    
    /**
     * Retourne l'inverse d'un nombre : 1 en param retourne -1
     * @param type $nb
     * @return string
     */
    public static function inv($nb) {
        return 'res';
    }

}
