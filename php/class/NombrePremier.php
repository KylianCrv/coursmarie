<?php

class NombrePremier {

    public static function isNombrePremier($nombre) {
//Pour i = 2, et i inférieur au nombre demandé, je regarde si mon nombre est divisible dans N.
        if ($nombre == 0 || $nombre == 1) {
            $var = 1;
        } else {
            for ($i = 2; $nombre <= $i; $i++) {
                if ($nombre % $i === 0) {
                    $var = 1;
                }
            }
        }


        if (isset($var)) { //Check si la variable existe.
            return "$nombre n'est pas un nombre premier";
        } else { //Elle n'existe pas, donc c'est premier.
            return "$nombre est un nombre premier";
        }
    }

}
